package com.strong.Demo.blog;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.strong.Demo.blog.BlogService;

public class BlogController extends Controller {
	
	static BlogService service = new BlogService();
	
	public void index() {
		setAttr("blogPage", service.paginate(getParaToInt(0, 1), 10));
		render("blog.html");
		
	}
	
	public void add() {
	}
	
	
	public void save() {
		service.save();
		redirect("/blog");
	}
	
	public void edit() {
		setAttr("blog", service.findById(getParaToInt()));
	}
	
	
	@Before(BlogValidator.class)
	public void update() {
		service.update();
		redirect("/blog");
	}
	
	public void delete() {
		service.deleteById(getParaToInt());
		redirect("/blog");
	}

}
