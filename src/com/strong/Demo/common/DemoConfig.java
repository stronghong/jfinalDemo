package com.strong.Demo.common;

import com.strong.Demo.common.model._MappingKit;
import com.jfinal.config.*;
import com.jfinal.template.Engine;
import com.strong.Demo.Contorller.helloController;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.strong.Demo.index.IndexController;
import com.strong.Demo.blog.BlogController;

public class DemoConfig extends JFinalConfig {
    
	//配置常量
	public void configConstant(Constants me) {
		//通过内置的PropKit 来读取配置文件
		PropKit.use("a_little_config.txt");
		me.setDevMode(true);
    }
	/**
	 * 配置路由
	 */
    public void configRoute(Routes me) {
    	
       me.add("/hello", helloController.class);
       me.add("/",IndexController.class,"/index");
       me.add("/blog",BlogController.class);
    }
    
    public void configPlugin(Plugins me) {
    	//
    	//创建DruidPlugin 数据连接池，需要引入 com.jfinal.plugin.druid.DruidPlugin 在（druid-1.0.29.jar里边）
    	DruidPlugin dp = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password"));
        me.add(dp);
        
     // 配置ActiveRecord插件
     	ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
     		// 所有映射在 MappingKit 中自动化搞定(通过自动生成方法根据表结构来生成)
     		_MappingKit.mapping(arp);
     		me.add(arp);
        
    }
    public void configInterceptor(Interceptors me) {}
    public void configHandler(Handlers me) {}
    
	
	/***
	 *配置模板 
	 ***/
	public void configEngine(Engine me) {
		me.addSharedFunction("/common/_layout.html");
		me.addSharedFunction("/common/_paginate.html");
	}
	
	public static DruidPlugin createDruidPlugin() {
		return new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
		
	}
	
    public static void main(String[] args) {
    	// eclipse 下的启动方式
    	JFinal.start("WebRoot", 8088, "/", 5);
    	
    	// IDEA 下的启动方式
    	// JFinal.start("src/main/webapp", 80, "/");
    }
}